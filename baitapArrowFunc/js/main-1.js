// Bài 1
const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];

// Load data color cho UI
let loadColorUI = () => {
    let contentHTML1 = "";
    let contentHTML2 = "";

    colorList.forEach(item => {
        let contentBtn1 = `
        <button onclick="doiMauNha('${item}')" class="color-button ${item} active"></button>
        `

        let contentBtn2 = `
        <button onclick="doiMauNha('${item}')" class="color-button ${item}"></button>
        `
        if(item == "pallet"){
            contentHTML1 += contentBtn1;
        } else{
            contentHTML2 += contentBtn2;
        }
        
    });

    document.getElementById("colorContainer").innerHTML = contentHTML1 + contentHTML2;
}

loadColorUI()

// Đổi màu ngôi nhà
const colorBtn = document.querySelectorAll(".color-button");
const houseColor = document.getElementById("house");

let doiMauNha = (color) => {
    // Tìm index của color
    let index = -1;
    colorList.forEach(item => {
        if(color == item){
            index = colorList.indexOf(`${color}`);
        }
    });
    
    // Xóa & thêm active vào btn
    // Xóa & thêm color vào house
    colorBtn.forEach(item => {
        item.classList.remove("active");
    });
    // remove tất cả các màu trong mảng có trong ID
    houseColor.classList.remove(...colorList);
    
    colorBtn[index].classList.add("active");
    houseColor.classList.add(`${color}`);
}
