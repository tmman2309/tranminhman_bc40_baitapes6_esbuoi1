// Bài 2
// Func tính chung (Dùng rest params)
let dtbFunc = (...diemList) => {
  let tong = 0;

  diemList.forEach((item) => {
    tong += item;
  });

  return (tong / diemList.length).toFixed(2);
};

// Button 1
let tinhDTBKhoi1 = () => {
  let diemToan = document.getElementById("inpToan").value;
  let diemLy = document.getElementById("inpLy").value;
  let diemHoa = document.getElementById("inpHoa").value;

  if (diemToan == "" || diemLy == "" || diemHoa == "") {
    document.getElementById("tbKhoi1").innerText = "NaN";
  } else {
    document.getElementById("tbKhoi1").innerHTML = dtbFunc(
      diemToan * 1,
      diemLy * 1,
      diemHoa * 1
    );
  }
};

// Button 2
let tinhDTBKhoi2 = () => {
  let diemVan = document.getElementById("inpVan").value;
  let diemSu = document.getElementById("inpSu").value;
  let diemDia = document.getElementById("inpDia").value;
  let diemEnglish = document.getElementById("inpEnglish").value;

  if (diemVan == "" || diemSu == "" || diemDia == "" || diemEnglish == "") {
    document.getElementById("tbKhoi2").innerText = "NaN";
  } else {
    document.getElementById("tbKhoi2").innerHTML = dtbFunc(
      diemVan * 1,
      diemSu * 1,
      diemDia * 1,
      diemEnglish * 1
    );
  }
};
